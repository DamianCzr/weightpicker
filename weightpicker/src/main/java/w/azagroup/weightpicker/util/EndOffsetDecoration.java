package w.azagroup.weightpicker.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EndOffsetDecoration extends RecyclerView.ItemDecoration {
    private int mEndOffset;

    public EndOffsetDecoration(int bottomOffset) {
        mEndOffset = bottomOffset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int dataSize = state.getItemCount();
        int position = parent.getChildAdapterPosition(view);
        if (dataSize > 0 && position == dataSize - 1) {
            outRect.set(0, 0, mEndOffset, 0);
        } else if (dataSize > 0 && position == 0) {
            outRect.set(mEndOffset, 0, 0, 0);
        } else {
            outRect.set(0, 0, 0, 0);
        }
    }
}