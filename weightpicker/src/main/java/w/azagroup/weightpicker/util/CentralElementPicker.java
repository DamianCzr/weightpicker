package w.azagroup.weightpicker.util;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import w.azagroup.weightpicker.HorizontalPicker;

public class CentralElementPicker extends LinearLayoutManager {
    private HorizontalPicker.onScrollStopListener onScrollStopListener = null;
    private boolean selectable = true;
    private View selectedView;
    private RecyclerView rv;

    public CentralElementPicker(Context context, int orientation, RecyclerView rv, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.rv = rv;

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (onScrollStopListener != null && selectedView != null) {
                    onScrollStopListener.selectedView(selectedView, getPosition(selectedView));
                }
            }
        });
    }

    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int orientation = getOrientation();
        if (orientation == HORIZONTAL) {
            selectable = true;
            int scrolled = super.scrollHorizontallyBy(dx, recycler, state);
            updateSelection();
            return scrolled;
        } else return 0;
    }

    public void firstSelection() {
        try {
            updateSelection();
            if (onScrollStopListener != null) {
                onScrollStopListener.selectedView(selectedView, getPosition(selectedView));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSelection() {
        View newf = findCentralItem();
        if (!newf.equals(selectedView)) {
            selectedView = newf;
            for (int i = 0; i < getChildCount(); i++) {
                View v = getChildAt(i);
                if (v.equals(selectedView)) {
                    v.setAlpha(1f);
                } else {
                    v.setAlpha(0.5f);
                }
            }
        }
    }

    public void setOnScrollStopListener(HorizontalPicker.onScrollStopListener onScrollStopListener) {
        this.onScrollStopListener = onScrollStopListener;
    }

    public int getItemCount() {
        return super.getItemCount();
    }

    public int findFirstVisibleItemPosition() {
        final View child = findSelectedChild(0, getChildCount());
        return child == null ? RecyclerView.NO_POSITION : rv.getChildAdapterPosition(child);
    }

    public View findCentralItem() {
        return findSelectedChild(0, getChildCount());
    }

    public int findFirstCompletelyVisibleItemPosition() {
        final View child = findSelectedChild(0, getChildCount());
        return child == null ? RecyclerView.NO_POSITION : rv.getChildAdapterPosition(child);
    }

    public View findFirstCompletelyVisibleItem() {
        return findSelectedChild(0, getChildCount());
    }

    View findSelectedChild(int fromIndex, int toIndex) {
        OrientationHelper helper;
        if (this.canScrollVertically()) {
            helper = OrientationHelper.createVerticalHelper(this);
        } else {
            helper = OrientationHelper.createHorizontalHelper(this);
        }

        final int start = 0;
        final int end = helper.getEnd();
        final int center = (end - start) / 2;
        final int next = toIndex > fromIndex ? 1 : -1;
        for (int i = fromIndex; i != toIndex; i += next) {
            final View child = this.getChildAt(i);
            final int childStart = helper.getDecoratedStart(child);
            final int childEnd = helper.getDecoratedEnd(child);
            if (childStart < center && childEnd >= center && selectable) {
                selectable = false;
                return child;
            }
        }
        return this.getChildAt(toIndex);
    }

    public void select(int position) {
        scrollToPositionWithOffset(position, 0);
        rv.smoothScrollBy(1, 0);
    }
}