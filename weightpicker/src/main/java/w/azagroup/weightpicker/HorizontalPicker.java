package w.azagroup.weightpicker;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import java.util.List;

import w.azagroup.weightpicker.model.LargeLabel;
import w.azagroup.weightpicker.util.CentralElementPicker;
import w.azagroup.weightpicker.util.EndOffsetDecoration;
import w.azagroup.weightpicker.util.StartSnapHelper;
import w.azagroup.weightpicker2.R;

public class HorizontalPicker extends RecyclerView {
    private SlimAdapter slimAdapter;
    private CentralElementPicker pickerLayoutManager;

    public HorizontalPicker(Context context) {
        super(context);
        init(context);
    }

    public HorizontalPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HorizontalPicker(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                try {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int w = getWidth() / 2;
                    addItemDecoration(new EndOffsetDecoration(w));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        StartSnapHelper startSnapHelper = new StartSnapHelper();
        startSnapHelper.attachToRecyclerView(this);

        pickerLayoutManager = new CentralElementPicker(context, LinearLayoutManager.HORIZONTAL, this, false);

        setLayoutManager(pickerLayoutManager);

        slimAdapter = SlimAdapter.create()
                .register(R.layout.weight_item_labeled_large, new SlimInjector<LargeLabel>() {
                    @Override
                    public void onInject(LargeLabel data, IViewInjector injector) {
                        injector.text(R.id.largeLabel, data.getLabel());
                    }
                })
                .register(R.layout.weight_item_labeled_medium, new SlimInjector<String>() {
                    @Override
                    public void onInject(String data, IViewInjector injector) {
                        injector.text(R.id.largeLabel, data);
                    }
                })
                .register(R.layout.weight_item, new SlimInjector<Integer>() {
                    @Override
                    public void onInject(Integer data, IViewInjector injector) {
                    }
                })
                .attachTo(this);
    }

    public void addData(List<?> data) {
        slimAdapter.updateData(data);
        post(() -> firstSelection());
    }

    public void addData(List<?> data, final int selectedItem) {
        slimAdapter.updateData(data);
        post(() -> selectItem(selectedItem));
    }

    public void setOnScrollStopListener(onScrollStopListener listener) {
        if (pickerLayoutManager != null) {
            pickerLayoutManager.setOnScrollStopListener(listener);
        }
    }

    public void firstSelection() {
        pickerLayoutManager.firstSelection();
    }

    public void selectItem(int position) {
        try {
            if (position < slimAdapter.getItemCount()) {
                pickerLayoutManager.select(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface onScrollStopListener {
        void selectedView(View view, int position);
    }
}
