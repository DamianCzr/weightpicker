package w.azagroup.weightpicker.model;

public class LargeLabel {
    private String label;

    public LargeLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
